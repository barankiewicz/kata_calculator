﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Kata_StringCalculator
{
    public static class StringCalculator
    {
        public static int Add(string numbers)
        {
            var digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-' };

            string[] delimiters = new string[2] { '\n'.ToString(), ','.ToString() };
            if (numbers.Length == 0) return 0;
            if (numbers.Length >= 2 && numbers.Substring(0, 2) == "//")
            {
                delimiters = GetDelimiters(numbers, digits);
                numbers = numbers.Substring(numbers.IndexOfAny(digits));
            }

            var delist = delimiters.ToList();
            delist.Add('\n'.ToString());
            delist.Add(','.ToString());
            delimiters = delist.ToArray();

            var nums = numbers.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

            var negativeNums = nums.Where(x => x < 0).ToList();
            if(negativeNums.Count > 0)
                throw new Exception("Negatives not allowed: " + String.Join(",", negativeNums.Select(x => x.ToString()).ToArray()));

            return nums.Where(x => x <= 1000).Sum();
        }

        public static string[] GetDelimiters(string numbers, char[] digits)
        {
            String delimiters = numbers.Substring(2, numbers.IndexOfAny(digits) - 3);

            var delims = delimiters.Split(new string[] { '['.ToString(), ']'.ToString() }, StringSplitOptions.RemoveEmptyEntries);
            return delims;
        }
    }
}
